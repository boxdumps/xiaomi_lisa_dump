## missi-user 11 RKQ1.210503.001 V12.5.5.0.RKOCNXM release-keys
- Manufacturer: xiaomi
- Platform: lahaina
- Codename: lisa
- Brand: Xiaomi
- Flavor: missi-user
- Release Version: 11
- Id: RKQ1.210503.001
- Incremental: V12.5.5.0.RKOCNXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Xiaomi/lisa/lisa:11/RKQ1.210503.001/V12.5.5.0.RKOCNXM:user/release-keys
- OTA version: 
- Branch: missi-user-11-RKQ1.210503.001-V12.5.5.0.RKOCNXM-release-keys
- Repo: xiaomi_lisa_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
